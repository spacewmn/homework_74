const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = "./messages";
let dataList = [];


router.get('/messages', (req, res) => {
    fs.readdir(path, (err, files) => {
        files.forEach(file => {
            const fileContents = fs.readFileSync(path + '/' + file)
            dataList.push(JSON.parse(fileContents));
        });
        if (dataList.length > 5) {
            let newDataList = dataList.splice(dataList.length - 5 , dataList.length - 1);
            res.send(newDataList);
        }
        res.send(dataList);
        console.log(dataList);

    })
});

router.post('/create', (req, res) => {
    const date = new Date().toJSON();
    const messages = {...req.body, date};
    fs.writeFile(path + '/' + messages.date + '.txt', JSON.stringify(messages), (err) => {
        if (err) {
            console.error(err);
        }
        console.log('File was saved!');
    });
    res.send(messages);
});

module.exports = router;